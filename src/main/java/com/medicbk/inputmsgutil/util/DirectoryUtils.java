package com.medicbk.inputmsgutil.util;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Component
@RequiredArgsConstructor
public class DirectoryUtils {
    private final XmlFileProcessor xmlFileProcessor;

    /**
     * Processes XML files in the specified directory.
     * @param directoryPath The path of the directory containing XML files.
     */
    public void processXmlFilesInDirectory(String directoryPath) {
        try (var files = Files.walk(Path.of(directoryPath))) {
            files
                    .filter(Files::isRegularFile)
                    .filter(this::isXmlFile)
                    .forEach(xmlFileProcessor::processXmlFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean isXmlFile(Path filePath) {
        String fileName = filePath.getFileName().toString();
        return fileName.endsWith(".xml");
    }
}
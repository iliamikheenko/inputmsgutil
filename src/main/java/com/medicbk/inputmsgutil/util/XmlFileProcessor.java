package com.medicbk.inputmsgutil.util;

import com.medicbk.inputmsgutil.parser.XmlParser;
import com.medicbk.inputmsgutil.service.InputMsgService;
import com.medicbk.inputmsgutil.service.ReportService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Slf4j
@Component
@RequiredArgsConstructor
public class XmlFileProcessor {
    private final XmlParser xmlParser;
    private final InputMsgService inputMsgService;
    private final ReportService reportService;

    /**
     * Process an XML file, extract information from it,
     * and save the data to the databases.
     *
     * @param filePath The path to the XML file to be processed.
     */
    public void processXmlFile(Path filePath) {
        try {
            String content = Files.readString(filePath);
            var xmlParserOpt = xmlParser.extractInfoFromXml(content);

            if (xmlParserOpt.isPresent()){
                var xmlParserDto = xmlParserOpt.get();
                var patientId = xmlParserDto.getPatientId();
                var patientUUID = xmlParserDto.getUuid();
                var nosologyIds = xmlParserDto.getNosologyIds();

                inputMsgService.createInstanceAndSave(patientUUID, patientId, nosologyIds);
                reportService.createInstanceAndSave(patientUUID,patientId,nosologyIds);
            } else {
                log.error("XML parsing returned no data. File: " + filePath);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
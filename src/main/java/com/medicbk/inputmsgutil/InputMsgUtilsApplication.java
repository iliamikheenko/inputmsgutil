package com.medicbk.inputmsgutil;

import com.medicbk.inputmsgutil.util.DirectoryUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@RequiredArgsConstructor
public class InputMsgUtilsApplication {

    private final DirectoryUtils directoryUtils;

    @Value("${source.path}")
    private String folderPath;

    public static void main(String[] args) {
        var context = SpringApplication.run(InputMsgUtilsApplication.class, args);
        var app = context.getBean(InputMsgUtilsApplication.class);
        app.processXmlFilesInDirectory();
    }

    private void processXmlFilesInDirectory() {
        directoryUtils.processXmlFilesInDirectory(folderPath);
    }
}
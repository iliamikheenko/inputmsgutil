package com.medicbk.inputmsgutil.service;

import com.medicbk.inputmsgutil.enums.InputMsgStatus;
import com.medicbk.inputmsgutil.model.InputMsg;
import com.medicbk.inputmsgutil.repository.InputMsgRepository;
import com.medicbk.inputmsgutil.util.MultiKey;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static java.time.temporal.ChronoUnit.HOURS;

@Service
@RequiredArgsConstructor
public class InputMsgService {
    private final InputMsgRepository inputMsgRepository;

    public void save(InputMsg inputMsg){
        inputMsgRepository.save(inputMsg);
    }

    public void createInstanceAndSave(UUID patientUUID, Long patientId, List<Long> nosologyIds){
        var currentTime = Instant.now();

        InputMsg inputMsg = InputMsg.builder()
                .uuid(patientUUID)
                .patientId(patientId)
                .status(InputMsgStatus.PROCESSED)
                .locale("ru")
                .data(nosologyIds)
                .createDate(currentTime)
                .expireDate(currentTime.plus(100, HOURS))
                .ignoreDataset(true)
                .username("user@medicbk.com")
                .multiKey(new MultiKey(patientId, nosologyIds).getMD5sum())
                .build();

        save(inputMsg);
    }
}
package com.medicbk.inputmsgutil.repository;


import com.medicbk.inputmsgutil.model.InputMsg;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InputMsgRepository extends JpaRepository<InputMsg, Long> {
}
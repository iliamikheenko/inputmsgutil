package com.medicbk.inputmsgutil.repository;


import com.medicbk.inputmsgutil.model.Report;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ReportRepository extends JpaRepository<Report, UUID> {
}
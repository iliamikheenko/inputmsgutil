package com.medicbk.inputmsgutil.model;

import com.medicbk.inputmsgutil.enums.InputMsgStatus;
import com.vladmihalcea.hibernate.type.array.ListArrayType;
import com.vladmihalcea.hibernate.type.basic.PostgreSQLHStoreType;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TypeDefs({
        @TypeDef(name = "json", typeClass = JsonStringType.class),
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class),
        @TypeDef(name = "list-array", typeClass = ListArrayType.class),
        @TypeDef(name = "hstore", typeClass = PostgreSQLHStoreType.class),
})
@Table(name = "input_msg", schema = "report_manager")
public class InputMsg extends AbstractEntity{
    private static final long serialVersionUID = 2171588862786385140L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "uuid")
    private UUID uuid;

    @Column(name = "patient_id")
    private Long patientId;

    @Column(name = "multi_key")
    private String multiKey;

    @Column(name = "locale")
    private String locale;

    @Column(name = "data")
    @Type(type = "jsonb")
    private List<Long> data = new ArrayList<>();

    @Column(name = "create_date")
    private Instant createDate;

    @Column(name = "expire_date")
    private Instant expireDate;

    @Column(name = "ignore_dataset")
    private Boolean ignoreDataset;

    @Column(name = "username")
    private String username;

    @Column(name = "hold_date")
    private Instant holdDate;

    @Column(name = "route_callback")
    private String routeCallback;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private InputMsgStatus status;
}
package com.medicbk.inputmsgutil.enums;

public enum InputMsgStatus {
    NEW,
    PROCESSED
}